#!/usr/bin/python3
import json
import uuid
import requests

from enum import Enum

CHECKS = []

class Status(Enum):
	"""
	Status represents check's detection status for a url.

	Attributes:
		CUSTOM (str): Indicates a custom 404 page.
		DEFAULT (str): Indicates a default 404 page.
		UNKNOWN (str): Indicates some issue while running the check.
	"""
	CUSTOM = 'custom'
	DEFAULT = 'default'
	UNKNOWN = 'unknown'

def registry(check_desc):
	def warp(check_func):
		CHECKS.append((check_desc, check_func))
	return warp

def run(url: str) -> tuple[str, str]:
	"""Runs the classifier checks to identify if it has a custom/default 404 page, 
	ensuring there are no redirects and the URL is valid.

	Args:
		url (str): Url to be checked.
	
	Returns: 
		run (generator): A generator that yields each check's description and the check's result status.

	Yields: 
		check, status (tuple(str, str)): A tuple containing the checks description and the checks result status.

	Raises:
		ValueError: A value error is raised if given url is invalid. 
	"""

	# Load all handlers
	__import__('checks')

	try:
		# We padding some random uuid that will never be a valid path
		resp = requests.get(url + '/' + str(uuid.uuid4()), allow_redirects=False, timeout=3)
	
	except requests.RequestException as e:
		raise ValueError(f'Invalid URL: {e}')

	for check, handler in CHECKS:
		yield check, handler(resp)
