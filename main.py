#!/usr/bin/python3
import argparse
import classifier
from collections import defaultdict

def parse_args():
	"""Creates arg parser to take url as input
	
	Returns:
		args: Arguments object containing the input.
	"""
	parser = argparse.ArgumentParser(description="The URL to run 404 checks on and state if it's a default/custom 404 page.")

	parser.add_argument('url', type=str, help='The URL to process')
	args = parser.parse_args()

	return args

def cli():
	"""
	CLI function that checks a URL for a custom or default 404 page.
	"""
	args = parse_args()

	print('Checking if {} has a custom or default 404 page\n'.format(args.url))

    # Store the checks outcome count
	outcomes = defaultdict(int)

    # Runs the classifier
	result = classifier.run(args.url)
	try:
		for check, status in result:
			print('CHECK: {} ... [{}]'.format(check, status.name))
			outcomes[status.name] += 1

        # Shows the probability including all checks outcome
		print('Probability for:')
		for status, count in outcomes.items():
			pp = (count / sum(outcomes.values())) * 100
			print(f"	{status}: {pp:.2f}%")

	except ValueError:
		print('The url provided is invalid.')

if __name__ == '__main__':
	cli()
