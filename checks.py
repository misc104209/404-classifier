#!/usr/bin/python3
from classifier import registry
from classifier import Status

@registry('Header contains `server`')
def check_header_server(response) -> Status:
	"""The most probable header `Server` that most default server running 404 pages show in their headers.

	Probability: HIGH
	"""
	if 'Server' in response.headers:
		return Status.DEFAULT
	return Status.CUSTOM


@registry('Site redirection')
def site_redirect(response) -> Status:
	"""Most default servers running pages dont do site redirection on a 404 page.

	Probability: MEDIUM
	"""
	if 'Location' in response.headers or (response.status_code < 400 and response.status_code >= 300):
		return Status.CUSTOM
	return Status.DEFAULT

@registry('Contains and loads <script> in page')
def check_script(response) -> Status:
	"""Default 404 pages normally dont need to run/load any javascript.
	"""
	if '<script' in response.text:
		return Status.CUSTOM
	return Status.DEFAULT

@registry('Contains the location of our random 404 path in body')
def check_path_in_page(response) -> Status:
	"""Default 404 pages normally dont contain the location we provided in body itself.
	"""
	if response.url.split('/')[-1] in response.text:
		return Status.CUSTOM
	return Status.DEFAULT

