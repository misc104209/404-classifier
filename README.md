# 404-classifier
This is a sample project just to check if a given url has a custom 404 page or a default 404 page setup.

## Installation
```
git clone https://gitlab.com/misc104209/404-classifier.git
pip install requirements.txt
```

## Usage
Heres some sample that you could try:
1. Contains a default 404 page:
	```
	python main.py https://goog.com
	```
2. Contains a custom 404 page:
	```
	python main.py https://google.com
	```

This should create an output like this:
```
└──╼ $python main.py http://127.0.0.1                                                                                                        
Checking if http://127.0.0.1 has a custom or default 404 page

CHECK: Header contains `server` ... [DEFAULT]
CHECK: Site redirection ... [DEFAULT]
CHECK: Contains and loads <script> in page ... [DEFAULT]
CHECK: Contains the location of our random 404 path in body ... [DEFAULT]
Probability for:
        DEFAULT: 100.00%
```
## Quick Note
To give a walkthrough of idea/research well I set up several demo servers for Apache, Nginx and OpenLiteSpeed locally using Docker. My goal was to pinpoint elements that could reveal whether a webpage is a default page or a custom 404 page by inspecting the page's elements.

A few things can be improved like run check in thread, add weights to each checks for more accuracy, mroe accurate checks using regex (like server version), and so on. Currently all checks are considered equal in determining the final outcome and also all checks are run sequentially.

As a demo project it still serves its purpose.
